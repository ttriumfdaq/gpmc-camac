/********************************************************************

  Name:         mcstd_libgpmc_camac.c
  Created by:   Konstantin Olchansk - TRIUMF DAQ

  Contents:     MIDAS mcstd.h driver for the MityARM based CAMAC controller

\********************************************************************/

#include <stdio.h>
#include <string.h>

/*------------------------------------------------------------------*/

#include "mcstd.h"
#include "libgpmc_camac.h"

/*---- global variables  -------------------------------------------*/

/*------------------------------------------------------------------*/

void cam24i_q(const int c, const int n, const int a, const int f, DWORD * d, int *x, int *q)
{
   int loop = 0;
   gpmc_camac_NFAWMXQRL_mmap(n, f, a, 0, 0, x, q, d, &loop);
   //printf("cam24i_q: c %d, n %d, a %d, f %d, loop %d, x %d, q %d\n", c, n, a, f, loop, *x, *q);
}

/*------------------------------------------------------------------*/

void cam24o_q(const int c, const int n, const int a, const int f, DWORD d, int *x, int *q)
{
   int loop = 0;
   DWORD rd;
   gpmc_camac_NFAWMXQRL_mmap(n, f, a, d, 0, x, q, &rd, &loop);
}

/*------------------------------------------------------------------*/

void cami(const int c, const int n, const int a, const int f, WORD * d)
{
   DWORD tmp;
   int x, q;
   cam24i_q(c, n, a, f, &tmp, &x, &q);
   *d = tmp;
}

/*------------------------------------------------------------------*/

void cam16i(const int c, const int n, const int a, const int f, WORD * d)
{
   cami(c, n, a, f, d);
}

/*------------------------------------------------------------------*/

void cam24i(const int c, const int n, const int a, const int f, DWORD * d)
{
   int x, q;
   cam24i_q(c, n, a, f, d, &x, &q);
}

/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/

void cam16i_q(const int c, const int n, const int a, const int f, WORD * d, int *x, int *q)
{
   DWORD tmp;
   cam24i_q(c, n, a, f, &tmp, x, q);
   *d = tmp;
}

/*------------------------------------------------------------------*/

void cam16i_r(const int c, const int n, const int a, const int f, WORD ** d, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      cam16i(c, n, a, f, (*d)++);
}

/*------------------------------------------------------------------*/

void cam24i_r(const int c, const int n, const int a, const int f, DWORD ** d, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      cam24i(c, n, a, f, (*d)++);
}

/*------------------------------------------------------------------*/

void cam16i_rq(const int c, const int n, const int a, const int f, WORD ** d, const int r)
{
   WORD dtemp;
   WORD csr, i;

   for (i = 0; i < r; i++) {
      cam16i(c, n, a, f, &dtemp);
      cami(0, 29, 0, 0, &csr);
      if ((csr & 0x8000) != 0)
         *((*d)++) = dtemp;
      else
         break;
   }
}

/*------------------------------------------------------------------*/

void cam24i_rq(const int c, const int n, const int a, const int f, DWORD ** d, const int r)
{
   DWORD i, dtemp;
   WORD csr;

   for (i = 0; i < (DWORD) r; i++) {
      cam24i(c, n, a, f, &dtemp);
      cami(0, 29, 0, 0, &csr);
      if ((csr & 0x8000) != 0)
         *((*d)++) = dtemp;
      else
         break;
   }
}

/*------------------------------------------------------------------*/

void cam16i_sa(const int c, const int n, const int a, const int f, WORD ** d, const int r)
{
   int aa;

   for (aa = a; aa < a + r; aa++)
      cam16i(c, n, aa, f, (*d)++);
}

/*------------------------------------------------------------------*/

void cam24i_sa(const int c, const int n, const int a, const int f, DWORD ** d, const int r)
{
   int aa;

   for (aa = a; aa < a + r; aa++)
      cam24i(c, n, aa, f, (*d)++);
}

/*------------------------------------------------------------------*/

void cam16i_sn(const int c, const int n, const int a, const int f, WORD ** d, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      cam16i(c, n + i, a, f, (*d)++);
}

/*------------------------------------------------------------------*/

void cam24i_sn(const int c, const int n, const int a, const int f, DWORD ** d, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      cam24i(c, n + i, a, f, (*d)++);
}

/*------------------------------------------------------------------*/

void camo(const int c, const int n, const int a, const int f, WORD d)
{
   cam24o(c, n, a, f, d);
}

/*------------------------------------------------------------------*/

void cam16o(const int c, const int n, const int a, const int f, WORD d)
{
   camo(c, n, a, f, d);
}

/*------------------------------------------------------------------*/

void cam24o(const int c, const int n, const int a, const int f, DWORD d)
{
   int x, q;
   cam24o_q(c, n, a, f, d, &x, &q);
}

/*------------------------------------------------------------------*/

void cam16o_q(const int c, const int n, const int a, const int f, WORD d, int *x, int *q)
{
   cam24o_q(c, n, a, f, d, x, q);
}

/*------------------------------------------------------------------*/

void cam24o_r(const int c, const int n, const int a, const int f, DWORD * d, const int r)
{
  int i;
  for (i=0; i<r; i++)
    cam24o(c,n,a,f,d[i]);
}

#if 0
/*------------------------------------------------------------------*/

int camc_chk(const int c)
{
   unsigned int adr, n, a, f;

   /* clear inhibit */
   camc(c, 1, 2, 32);

   /* read back naf */
   adr = MAKE_BASE(c);
   a = (BYTE) INP(adr + 10);
   n = (BYTE) INP(adr + 10);
   f = (BYTE) INP(adr + 10);

   if (n != 1 || a != 2 || f != 32)
      return -1;

   return 0;
}
#endif

/*------------------------------------------------------------------*/

void camc(const int c, const int n, const int a, const int f)
{
   int q;
   camc_q(c, n, a, f, &q);
}

/*------------------------------------------------------------------*/

void camc_q(const int c, const int n, const int a, const int f, int *q)
{
   /* Following the CBD8210 manual */
   WORD dtmp;
   
   if (f < 16)
      cam16i(c, n, a, f, &dtmp);
   else if (f < 24)
      cam16o(c, n, a, f, 0);
   else
      cam16i(c, n, a, f, &dtmp);
}

/*------------------------------------------------------------------*/

void camc_sa(const int c, const int n, const int a, const int f, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      camc(c, n, a + i, f);
}

/*------------------------------------------------------------------*/

void camc_sn(const int c, const int n, const int a, const int f, const int r)
{
   int i;

   for (i = 0; i < r; i++)
      camc(c, n + i, a, f);
}

/*------------------------------------------------------------------*/

void cam_inhibit_set(const int c)
{
  printf("cam_inhibit_set: NOT IMPLEMENTED!\n");
  //unsigned int adr;
  //
  // adr = MAKE_BASE(c);
  // OUTP(adr + 10, 33);
}

/*------------------------------------------------------------------*/

void cam_inhibit_clear(const int c)
{
  printf("cam_inhibit_clear: NOT IMPLEMENTED!\n");
  // unsigned int adr;
  //
  // adr = MAKE_BASE(c);
  // OUTP(adr + 10, 32);
}

#if 0
/*------------------------------------------------------------------*/

int cam_inhibit_test(const int c)
{
   unsigned int adr;
   BYTE status;

   adr = MAKE_BASE(c);
   status = (BYTE) INP(adr + 6);
   return (status & 1) > 0;
}
#endif

/*------------------------------------------------------------------*/

void cam_crate_clear(const int c)
{
  printf("cam_crate_clear: NOT IMPLEMENTED!\n");
  // unsigned int adr;
  //
  // adr = MAKE_BASE(c);
  // OUTP(adr + 10, 36);
}

/*------------------------------------------------------------------*/

void cam_crate_zinit(const int c)
{
  printf("cam_crate_zinit: NOT IMPLEMENTED!\n");
  // unsigned int adr;
  //
  // adr = MAKE_BASE(c);
  // OUTP(adr + 10, 34);
}

/*------------------------------------------------------------------*/

void cam_lam_enable(const int c, const int n)
{
  printf("cam_lam_enable: NOT IMPLEMENTED!\n");

#if 0
   /* enable the station number */
   unsigned int adr;

   /* enable LAM in controller */
   adr = MAKE_BASE(c);
   OUTP(adr + 10, 64 + n);
#endif
}

/*------------------------------------------------------------------*/

void cam_lam_disable(const int c, const int n)
{
  printf("cam_lam_disable: NOT IMPLEMENTED!\n");

#if 0
   /* disable the station number */
   unsigned int adr;

   /* disable LAM in controller */
   adr = MAKE_BASE(c);
   OUTP(adr + 10, 128 + n);
#endif
}

#if 0
/*------------------------------------------------------------------*/

void cam_interrupt_enable(const int c)
{
   unsigned int adr;

   /* enable interrupts in controller */
   adr = MAKE_BASE(c);
   OUTP(adr + 10, 41);
}

/*------------------------------------------------------------------*/

void cam_interrupt_disable(const int c)
{
   unsigned int adr;

   /* disable interrupts in controller */
   adr = MAKE_BASE(c);
   OUTP(adr + 10, 40);
}

/*------------------------------------------------------------------*/

int cam_interrupt_test(const int c)
{
   unsigned int adr;
   BYTE status;

   adr = MAKE_BASE(c);
   status = (BYTE) INP(adr + 6);
   return (status & (1 << 2)) > 0;
}
#endif

/*------------------------------------------------------------------*/

void cam_lam_read(const int c, DWORD * lam)
{
  uint32_t reg7 = gpmc_camac_readReg32(7);

  static uint32_t xlam = 0;

  if (reg7 != xlam) {
    printf("cam_lam_read: reg7: 0x%08x -> 0x%08x\n", xlam, reg7);
    xlam = reg7;
  }
  *lam = 0;
#if 0
   /*
      return a BITWISE coded station NOT the station number
      i.e.: n = 5  ==> lam = 0x10
    */
   unsigned int adr, csr;

   adr = MAKE_BASE(c);
   csr = (BYTE) INP(adr + 6);
   if (csr & (1 << 3)) {
      *lam = ((BYTE) INP(adr + 8)) & 0x1F;      // mask upper 3 bits
      *lam = 1 << (*lam - 1);
   } else
      *lam = 0;
#endif
}

/*------------------------------------------------------------------*/

void cam_lam_clear(const int c, const int n)
{
  static int once = 1;
  if (once) {
    once = 0;
    printf("cam_lam_clear: NOT IMPLEMENTED!\n");
  }

#if 0
   unsigned int adr;

   /*
      note that the LAM flip-flop in unit must be cleared via

      camc(c, n, 0, 10);

      in the user code prior to the call of cam_lam_clear()
    */

   /* restart LAM scanner in controller */
   adr = MAKE_BASE(c);
   INP(adr + 8);
#endif
}

/*------------------------------------------------------------------*/

int cam_init_rpc(const char *host_name, const char *exp_name, const char *fe_name, const char *client_name, const char *rpc_server)
{
   /* dummy routine for compatibility */
   return 1;
}

/*------------------------------------------------------------------*/

int cam_init(void)
{
   int err;

   err = gpmc_open();
   if (err != 0) {
      printf("Cannot open GPMC interface, gpmc_open() returned %d\n", err);
      return 0;
   }

   return SUCCESS;
}

/*------------------------------------------------------------------*/

void cam_exit(void)
{
   gpmc_close();
}

/*------------------------------------------------------------------*/
void cam_op()
{
}

#if 0
int main(int argc, char* argv[])
{
   int err;

   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   err = gpmc_open();
   if (err != 0) {
      printf("Cannot open GPMC interface, gpmc_open() returned %d\n", err);
      exit(1);
   }

   if (argc == 2 && strcmp(argv[1], "-h")==0) {
      usage();
   }

   if (argc == 2 && strcmp(argv[1], "--reboot")==0) {
      printf("CAMAC firmware: 0x%08x\n", gpmc_camac_readReg32(0));
      printf("rebooting FPGA...\n");
      gpmc_camac_writeReg32(1, 2); // command 2 reboot
      sleep(1);
      printf("CAMAC firmware: 0x%08x\n", gpmc_camac_readReg32(0));
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--scan")==0) {
      printf("Scan of CAMAC crate:\n");
      for (int n=1; n<=24; n++) {
         int x = 0;
         int q = 0;
         uint32_t read_data = 0;
         int loop = 0;
#if 0
         gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
         if (x || q)
            printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
         loop = 0;
         gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
         if (x || q)
            printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
         //sleep(1);
#endif
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--slot23")==0) {
      int n = 23;
      int x = 0;
      int q = 0;
      uint32_t read_data = 0;
      int loop = 0;
#if 1
      gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
      loop = 0;
      gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
#endif
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--slot24")==0) {
      int n = 24;
      int x = 0;
      int q = 0;
      uint32_t read_data = 0;
      int loop = 0;
#if 1
      gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
      loop = 0;
      gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
#endif
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--test")==0) {
      while (1) {
         for (int write_data = 0; ; write_data++) {
            int n = 22;
            int x = 0;
            int q = 0;
            uint32_t read_data = 0;
            int loop = 0;
            gpmc_camac_NFAWMXQRL(n, 16, 0, ~write_data, 0, &x, &q, &read_data, &loop);
            int print = 0;
            if ((write_data%100000) == 0)
               print = 1;
            if (!q || !x)
               print = 1;
            if (loop > 4)
               print = 1;
            if (print)
               printf("N=%2d, X=%d, Q=%d, write=0x%08x, read=0x%08x, loop=%d\n", n, x, q, write_data, read_data, loop);
         }
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--gpmctest1")==0) {
      int ireg = 2;
      int errors = 0;
      for (int iloop=0; ; iloop++) {
         uint32_t w = random();
         gpmc_camac_writeReg32(2, w);
         uint32_t r = gpmc_camac_readReg32(2);
         uint32_t diff = w^r;
         if (diff != 0)
            errors++;
         if ((iloop==0) || diff || (iloop%1000000 == 0)) {
            printf("gpmctest register %d, loop %d, write 0x%08x, read 0x%08x, diff 0x%08x, errors %d\n", ireg, iloop, w, r, diff, errors);
            sleep(1);
         }
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--gpmctest2")==0) {
      int errors = 0;
      for (int iloop=0; ; iloop++) {
         uint32_t a = random();
         a &= 0x0FF8; // mask out: the top 4 bits is the register address, the low 2 bits are inaccessible, bit A[2] enables CAMAC operation.
         a |= (4<<12); // write to register 4
         uint32_t w = random();
         gpmc_camac_write32(a, w);
         uint32_t r4 = gpmc_camac_readReg32(4);
         uint32_t r5 = gpmc_camac_readReg32(5);
         a |= 2; // address bit 2 is always set (a 32-bit write is done as 2 16-bit writes to addresses a+0 and a+2. The last address latched.
         uint32_t diffa = a^r5;
         if (diffa != 0)
            errors++;
         uint32_t diffd = w^r4;
         if (diffd != 0)
            errors++;
         if ((iloop==0) || diffa || diffd || (iloop%1000000 == 0)) {
            printf("gpmctest camac registers, loop %d, write 0x%04x 0x%08x, read 0x%04x 0x%08x, diff 0x%04x 0x%08x, errors %d\n", iloop, a, w, r5, r4, diffa, diffd, errors);
            sleep(1);
         }
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--testleds")==0) {
      for (int iloop=0; ; iloop++) {
         printf("LEDs on!\n");
         gpmc_camac_writeReg32(1, 0xFFFF0007);
         sleep(1);
         printf("LEDs off!\n");
         gpmc_camac_writeReg32(1, 0x00000007);
         sleep(1);
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--testlemo")==0) {
      for (int iloop=0; ; iloop++) {
         printf("LEMO inverted!\n");
         gpmc_camac_writeReg32(1, 0x80000008);
         sleep(1);
         printf("LEMO normal!\n");
         gpmc_camac_writeReg32(1, 0x00000008);
         sleep(1);
      }
      return 0;
   }

   if (argc == 1)
      {
         printf("register0: 0x%08x - CAMAC firmware revision\n", gpmc_camac_readReg32(0));
         printf("register1: 0x%08x - command register\n", gpmc_camac_readReg32(1));
         printf("register2: 0x%08x - read/write test register\n", gpmc_camac_readReg32(2));
         printf("register3: 0x%08x - flash programmer\n", gpmc_camac_readReg32(3));
         printf("register4: 0x%08x - CAMAC data readback\n", gpmc_camac_readReg32(4));
         printf("register5: 0x%08x - CAMAC address readback\n", gpmc_camac_readReg32(5));
         uint32_t r6 = gpmc_camac_readReg32(6);
         printf("register6: 0x%08x - CAMAC read data (0x%06x) and status (0x%02x)\n", r6, r6&0xFFFFFF, (r6>>24)&0xFF);
         uint32_t r7 = gpmc_camac_readReg32(7);
         printf("register7: 0x%08x - CAMAC LAM (0x%06x) and status (0x%02x)\n", r7, r7&0xFFFFFF, (r7>>24)&0xFF);
      }
   else if (argc == 2)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         //printf("addr 0x%08x\n", i);

         uint32_t v = gpmc_camac_readReg32(i);
         printf("camac register %d: 0x%08x\n", i, v);
         
      }
   else if (argc == 3)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         uint32_t v = strtoul(argv[2], NULL, 0);
         printf("write camac register %d, value 0x%08x\n", i, v);
         
         gpmc_camac_writeReg32(i, v);
      }
   else if (argc == 5)
      {
         int n = strtoul(argv[1], NULL, 0);
         int f = strtoul(argv[2], NULL, 0);
         int a = strtoul(argv[3], NULL, 0);
         int w = strtoul(argv[4], NULL, 0);

         int x = 0;
         int q = 0;
         uint32_t read_data = 0;
         int loop = 0;
         gpmc_camac_NFAWMXQRL(n, f, a, w, 0, &x, &q, &read_data, &loop);
         printf("N=%d, F=%d, A=%d, W=0x%08x, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, f, a, w,  x, q, read_data, loop);
      }

   return 0;
}
#endif

// end

