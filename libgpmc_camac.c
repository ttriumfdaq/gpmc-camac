#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "libgpmc_camac.h"

static const uint32_t data_addr = 0x09000000; // address of CS1. access to CS0 at 0x8... is not permitted.
static const uint32_t ctrl_addr = 0x44E10000;
static const uint32_t gpmc_addr = 0x50000000;
static const uint32_t camac_addr = 0x0B000000; // address of CS3

static const int data_size = 3*0x01000000;
static const int regs_size = 0x2000;
static const int ctrl_size = 0x2000;
static const int camac_size = 0x01000000;

static int   gpmc_fd = -1;

static char *ctrl_regs;
static char *gpmc_regs;
static char *gpmc_data;
static char *camac_data;

// read and write arbitrary 32-bit registers (control module, gpmc control, gpmc data)

static void gpmc_writeReg32(char* base, uint32_t offset, uint32_t value)
{
   *(volatile uint32_t*)(base + offset) = value;
}

static uint32_t gpmc_readReg32(char* base, uint32_t offset)
{
   return *(volatile uint32_t*)(base + offset);
}

// read and write 32-bit data through the GPMC bus

void gpmc_write32(int addr, uint32_t value)
{
   *(volatile uint32_t*)(gpmc_data + addr - data_addr) = value;
}

uint32_t gpmc_read32(int addr)
{
   return *(volatile uint32_t*)(gpmc_data + addr - data_addr);
}

// read and write 32-bit data from registers in the CAMAC interface

void gpmc_camac_write32(uint32_t avalue, uint32_t dvalue)
{
   avalue &= 0x1FFFE;
   *(volatile uint32_t*)(camac_data + avalue) = dvalue;
}

void gpmc_camac_writeReg32(int ireg, uint32_t value)
{
   *(volatile uint32_t*)(camac_data + (ireg<<12)) = value;
}

uint32_t gpmc_camac_readReg32(int ireg)
{
   return *(volatile uint32_t*)(camac_data + (ireg<<12));
}

// CAMAC cycle function

void gpmc_camac_NFAWMXQRL_mmap(int n, int f, int a, int write_data, int loop_max, int* x, int* q, uint32_t *read_data, int* loop)
{
   int i;
   int avalue = 0;
   uint32_t dvalue = 0;
   uint32_t rvalue = 0;
   uint32_t rr = 0;

   n &= 0x1F;
   f &= 0x1F;
   a &= 0xF;
   write_data &= 0xFFFFFF;

   avalue |= 0x4000;
   avalue |= (n<<4);
   avalue |= (f&0x10)>>1; // F16
   avalue |= 1<<2; // "go" bit

   dvalue |= write_data;
   dvalue |= a<<24;
   dvalue |= (f&0xF)<<28;

   //printf("camac operation 0x%08x 0x%08x\n", avalue, dvalue);

   gpmc_camac_write32(avalue, dvalue);

   //rr = gpmc_camac_readReg32(6);

   if (loop_max == 0)
      loop_max = 100;

   for (i=1; i<loop_max; i++) {
      rvalue = gpmc_camac_readReg32(6);
      if (0) {
         if (rvalue != rr)
            printf("loop %d rvalue 0x%08x\n", i, rvalue);
         rr = rvalue;
      }
      // old DONE bit
      //if ((rvalue & 0x04000000)) 
      //   break;
      // to look at camac_done bit ** KL 2015-03-11
      if ((rvalue & 0x80000000)) 
         break;
   }

   if (x) {
      if (rvalue & (1<<25))
         *x = 1;
      else
         *x = 0;
   }

   if (q) {
      if (rvalue & (1<<24))
         *q = 1;
      else
         *q = 0;
   }

   if (loop)
      *loop = i;

   if (read_data)
      *read_data = rvalue;
}

void gpmc_camac_NFAWMXQRL(int n, int f, int a, int write_data, int loop_max, int* x, int* q, uint32_t *read_data, int* loop)
{
   int status;
   uint32_t buf[100];
   uint32_t* bufp = buf;
   uint32_t* resp;
   *bufp++ = n;
   *bufp++ = f;
   *bufp++ = a;
   *bufp++ = write_data;
   *bufp++ = loop_max;
   resp = bufp;
   *bufp++ = 0; // x
   *bufp++ = 0; // q
   *bufp++ = 0; // read_data
   *bufp++ = 0; // loop counter

   ioctl(gpmc_fd, 1<<16|((char*)bufp-(char*)buf), buf);

   if (x)
      *x = resp[0];
   if (q)
      *q = resp[1];
   if (read_data)
      *read_data = resp[2];
   if (loop)
      *loop = resp[3];
}

// service functions follow

static int printReg(const char* name, uint32_t offset, char* base)
{
   //uint32_t value = *(uint32_t*)(base+offset);
   uint32_t value = gpmc_readReg32(base, offset);
   printf("offset 0x%04x: %-25s: 0x%08x\n", offset, name, value);
   return value;
}

static int printPadReg(const char* name, uint32_t offset, char* base)
{
   //uint32_t value = *(uint32_t*)(base+offset);
   uint32_t value = gpmc_readReg32(base, offset);
   printf("offset 0x%04x: %-25s: 0x%08x: ", offset, name, value);
   printf("func %d", value & 0x7);
   printf(", PULLUDEN: %d", value & (1<<3));
   printf(", PULLTYPESEL: %d", value & (1<<4));
   printf(", RXACTIVE: %d", value & (1<<5));
   printf(", SLEWCTRL: %d", value & (1<<6));
   printf("\n");
   return value;
}

void gpmc_print_status()
{
   int i;
   int cs;

   printf("Control Module registers:\n");

   printReg("control_revision", 0, ctrl_regs);
   printReg("device_id", 0x600, ctrl_regs);
   printReg("control_hwinfo", 0x4, ctrl_regs);
   printReg("control_sysconfig", 0x10, ctrl_regs);
   printReg("control_status", 0x40, ctrl_regs);

#if 0
   // http://ladd00/~olchansk/MityARM/Docs/spruh73g.pdf

Table 9-1. Pad Control Register Field Descriptions

Bit Field Value Description
31-7 Reserved Reserved. Read returns 0.
6 SLEWCTRL Select between faster or slower slew rate.
  0 Fast
  1 Slow(1)
5 RXACTIVE Input enable value for the Pad. Set to 0 for output only. Set to 1 for input or output.
  0 Receiver disabled
  1 Receiver enabled
4 PULLTYPESEL Pad pullup/pulldown type selection
  0 Pulldown selected
  1 Pullup selected
3 PULLUDEN Pad Pullup/pulldown enable
  0 Pullup/pulldown enabled.
  1 Pullup/pulldown disabled.
2-0 MUXMODE Pad functional signal mux select

addresses of GPMC pinmux registers:

800h..83Ch conf_gpmc_ad0..ad15 Section 9.3.50
840h..86Ch conf_gpmc_a0..a11 Section 9.3.50
870h conf_gpmc_wait0 Section 9.3.50
874h conf_gpmc_wpn Section 9.3.50
878h conf_gpmc_ben1 Section 9.3.50
87Ch conf_gpmc_csn0
880h conf_gpmc_csn1 Section 9.3.50
884h conf_gpmc_csn2 Section 9.3.50
888h conf_gpmc_csn3 Section 9.3.50
88Ch conf_gpmc_clk Section 9.3.50
890h conf_gpmc_advn_ale Section 9.3.50
894h conf_gpmc_oen_ren Section 9.3.50
898h conf_gpmc_wen Section 9.3.50
89Ch conf_gpmc_ben0_cle

#endif

   for (i=0x800; i<=0x89C; i+=4) {
      printPadReg("config_gpmc", i, ctrl_regs);
   }

   if (0) {
      gpmc_writeReg32(ctrl_regs, 0x0820, 0x30);
      gpmc_writeReg32(ctrl_regs, 0x088C, 0x30);
   }

   printf("gpmc registers:\n");

   printReg("GPMC_REVISION", 0, gpmc_regs);
   printReg("GPMC_SYSCONFIG", 0x10, gpmc_regs);
   printReg("GPMC_SYSSTATUS", 0x14, gpmc_regs);
   printReg("GPMC_IRQSTATUS", 0x18, gpmc_regs);
   printReg("GPMC_IRQENABLE", 0x1C, gpmc_regs);
   printReg("GPMC_TIMEOUT_CONTROL", 0x40, gpmc_regs);
   printReg("GPMC_ERR_ADDRESS", 0x44, gpmc_regs);
   printReg("GPMC_ERR_TYPE", 0x48, gpmc_regs);
   printReg("GPMC_CONFIG", 0x50, gpmc_regs);
   printReg("GPMC_STATUS", 0x54, gpmc_regs);
   printReg("GPMC_PREFETCH_CONFIG1", 0x1e0, gpmc_regs);
   printReg("GPMC_PREFETCH_CONFIG2", 0x1e4, gpmc_regs);
   printReg("GPMC_PREFETCH_CONTROL", 0x1ec, gpmc_regs);
   printReg("GPMC_PREFETCH_STATUS", 0x1f0, gpmc_regs);

   for (cs=0; cs<=6; cs++)
      {
         printf("GPMC CS%d:\n", cs);
         printReg("GPMC_CONFIG1_i", 0x60 + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG2_i", 0x64 + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG3_i", 0x68 + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG4_i", 0x6C + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG5_i", 0x70 + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG6_i", 0x74 + (0x30*cs), gpmc_regs);
         printReg("GPMC_CONFIG7_i", 0x78 + (0x30*cs), gpmc_regs);
      }
}

int gpmc_init()
{
   if (1) {
      gpmc_writeReg32(gpmc_regs, 0x040, ((0x1FF<<4)|1)); // enable timeout
   }

   if (1)
      {
         int cs = 1;

         uint32_t reg1 = 0;
         reg1 |= (1<<29); // readtype: 0=async, 1=sync
         reg1 |= (1<<27); // writetype: 0=async, 1=sync
         reg1 |= (1<<25); // CLK activation time
         reg1 |= (1<<12); // 0=8bit, 1=16bit
         reg1 |= (1<<8); // 0=nonmux, 1=AADmux, 2=ADmux, 3=reserved
         reg1 |= (0<<0); // CLK divider

         uint32_t reg2 = 0;
         reg2 |= (0x3<<0); // CS ON time clocks
         reg2 |= (0x7<<8); // READ CS OFF time clocks
         reg2 |= (0x7<<16); // WRITE CS OFF time clocks

         uint32_t reg3 = 0;
         reg3 |= (2<<28);   // ADV_AAD_MUX_WR_OFF_TIME, ADV off time, AAD write
         reg3 |= (2<<24);   // ADV_AAD_MUX_RD_OFF_TIME, ADV off time, AAD read
         reg3 |= (4<<16);  // ADV_WR_OFF_TIME, ADV off time, write
         reg3 |= (4<<8);   // ADV_RD_OFF_TIME, ADV off time, read
         reg3 |= (1<<7);    // ADV half-clock delay
         reg3 |= (1<<4);    // ADV_AAD_MUX_ON_TIME, ADV on time, AAD cycle
         reg3 |= (3<<0);    // ADV_ON_TIME, ADV on time, normal cycle

         uint32_t reg4 = 0;
         reg4 |= (7<<24);  // WE_OFF_TIME
         reg4 |= (1<<23);   // WE half-clock delay
         reg4 |= (6<<16);  // WE_ON_TIME
         reg4 |= (2<<13);   // OE_AAD_MUX_OFF_TIME, OE AAD MUX off time
         reg4 |= (14<<8);    // OE_OFF_TIME, OE off time
         reg4 |= (1<<7);    // OE half-clock delay
         reg4 |= (0<<4);    // OE_AAD_MUX_ON_TIME, OE AAD MUX on time
         reg4 |= (10<<0);    // OE_ON_TIME, OE on time

         uint32_t reg5 = 0;
         reg5 |= (0x7<<24); // page burst access time
         reg5 |= (10<<16); // read access time
         reg5 |= (8<<8); // write cycle time
         reg5 |= (31<<0); // read cycle time

         uint32_t reg6 = 0;
         reg6 |= (5<<16); // WR_DATA_ON_AD_MUX_BUS, write data on AD mux bus

         gpmc_writeReg32(gpmc_regs,  0x60 + (0x30*cs), reg1); // config1
         gpmc_writeReg32(gpmc_regs,  0x64 + (0x30*cs), reg2); // config2
         gpmc_writeReg32(gpmc_regs,  0x68 + (0x30*cs), reg3); // config3
         gpmc_writeReg32(gpmc_regs,  0x6C + (0x30*cs), reg4); // config4
         gpmc_writeReg32(gpmc_regs,  0x70 + (0x30*cs), reg5); // config5
         gpmc_writeReg32(gpmc_regs,  0x74 + (0x30*cs), reg6); // config6
         gpmc_writeReg32(gpmc_regs,  0x78 + (0x30*cs), 0x00000f49); // config7
      }

   if (1)
      {
         int cs = 2;

         uint32_t reg1 = 0;
         reg1 |= (0<<30); // READ_MULTIPLE
         reg1 |= (0<<29); // readtype: 0=async, 1=sync
         reg1 |= (0<<28); // WRITE_MULTIPLE
         reg1 |= (1<<27); // writetype: 0=async, 1=sync
         reg1 |= (1<<25); // CLK activation time
         reg1 |= (0<<23); // attached device burst length
         reg1 |= (1<<12); // 0=8bit, 1=16bit
         reg1 |= (2<<8); // 0=nonmux, 1=AADmux, 2=ADmux, 3=reserved
         reg1 |= (0<<0); // CLK divider

         // timing of address phase
         int csontime = 0;
         int advontime = csontime + 1;
         int advofftime = advontime + 1;

         // timing of write cycle
         int wrdataonadmuxbus = advofftime + 0;
         int weontime = wrdataonadmuxbus + 1;
         int weofftime = weontime + 1;
         int wrcsofftime = weofftime + 0;
         int wrcycletime = wrcsofftime + 1;

         // timing of read cycle
         int oeontime = advofftime + 1;
         int rdaccesstime = oeontime + 3;
         int oeofftime = rdaccesstime + 1;
         int rdcsofftime = oeofftime + 0;
         int rdcycletime = rdcsofftime + 1;

         printf("CS %d, read cycle %d GPMC clocks, write cycle %d GPMC clocks\n", cs, rdcycletime, wrcycletime);

         uint32_t reg2 = 0;
         reg2 |= (csontime<<0); // CS ON time clocks
         reg2 |= (rdcsofftime<<8); // READ CS OFF time clocks
         reg2 |= (wrcsofftime<<16); // WRITE CS OFF time clocks

         uint32_t reg3 = 0;
         reg3 |= (0<<28);   // ADV_AAD_MUX_WR_OFF_TIME, ADV off time, AAD write
         reg3 |= (0<<24);   // ADV_AAD_MUX_RD_OFF_TIME, ADV off time, AAD read
         reg3 |= (advofftime<<16);  // ADV_WR_OFF_TIME, ADV off time, write
         reg3 |= (advofftime<<8);   // ADV_RD_OFF_TIME, ADV off time, read
         reg3 |= (1<<7);    // ADV half-clock delay
         reg3 |= (0<<4);    // ADV_AAD_MUX_ON_TIME, ADV on time, AAD cycle
         reg3 |= (advontime<<0);    // ADV_ON_TIME, ADV on time, normal cycle

         uint32_t reg4 = 0;
         reg4 |= (weofftime<<24);  // WE_OFF_TIME
         reg4 |= (1<<23);   // WE half-clock delay
         reg4 |= (weontime<<16);  // WE_ON_TIME
         reg4 |= (0<<13);   // OE_AAD_MUX_OFF_TIME, OE AAD MUX off time
         reg4 |= (oeofftime<<8);    // OE_OFF_TIME, OE off time
         reg4 |= (1<<7);    // OE half-clock delay
         reg4 |= (0<<4);    // OE_AAD_MUX_ON_TIME, OE AAD MUX on time
         reg4 |= (oeontime<<0);    // OE_ON_TIME, OE on time

         uint32_t reg5 = 0;
         reg5 |= (1<<24); // PAGE_BURST_ACCESS_TIME
         reg5 |= (rdaccesstime<<16); // read access time
         reg5 |= (wrcycletime<<8); // write cycle time
         reg5 |= (rdcycletime<<0); // read cycle time

         uint32_t reg6 = 0;
         reg6 |= (wrdataonadmuxbus<<16); // WR_DATA_ON_AD_MUX_BUS, write data on AD mux bus

         gpmc_writeReg32(gpmc_regs,  0x60 + (0x30*cs), reg1); // config1
         gpmc_writeReg32(gpmc_regs,  0x64 + (0x30*cs), reg2); // config2
         gpmc_writeReg32(gpmc_regs,  0x68 + (0x30*cs), reg3); // config3
         gpmc_writeReg32(gpmc_regs,  0x6C + (0x30*cs), reg4); // config4
         gpmc_writeReg32(gpmc_regs,  0x70 + (0x30*cs), reg5); // config5
         gpmc_writeReg32(gpmc_regs,  0x74 + (0x30*cs), reg6); // config6
         gpmc_writeReg32(gpmc_regs,  0x78 + (0x30*cs), 0x00000f48+cs); // config7
      }

   if (1)
      {
         int cs = 3;

         uint32_t reg1 = 0;
         reg1 |= (0<<31); // WRAP_BURST
         reg1 |= (1<<30); // READ_MULTIPLE
         reg1 |= (1<<29); // readtype: 0=async, 1=sync
         reg1 |= (0<<28); // WRITE_MULTIPLE
         reg1 |= (1<<27); // writetype: 0=async, 1=sync
         reg1 |= (0<<25); // CLK activation time
         reg1 |= (0<<23); // attached device burst length
         reg1 |= (1<<12); // 0=8bit, 1=16bit
         reg1 |= (2<<8); // 0=nonmux, 1=AADmux, 2=ADmux, 3=reserved
         reg1 |= (0<<0); // CLK divider

         int wraccesstime = 0; // not used?
         int weontime = 0; // not used?
         int weofftime = 0; // not used?

         // timing of address phase
         int csontime = 0;
         int csdelay = 0; // CS 1/2 clock delay
         int advontime = csontime + 1;
         int advdelay = 0; // ADV 1/2 clock delay

         // timing of write cycle
         int wrdataonadmuxbus = csontime + 1; // switch from driving address to driving data
         int wradvofftime = wrdataonadmuxbus + 1;
         int wrcsofftime = wrdataonadmuxbus + 1;
         int wrcycletime = wrcsofftime + 0;

         // timing of read cycle
         int oeontime = csontime + 1; // switch from driving address to reading data (GPMC bus turn around)
         int oedelay = 1; // delay OE by 1/2 clock
         int rdaccesstime = oeontime + 4;
         int oeofftime = rdaccesstime + 0;
         int rdcsofftime = oeofftime + 1;
         int rdadvofftime = rdcsofftime + 0;
         int rdcycletime = rdcsofftime + 0;

         printf("CS %d, read cycle %d GPMC clocks, write cycle %d GPMC clocks\n", cs, rdcycletime, wrcycletime);

         uint32_t reg2 = 0;
         reg2 |= (csontime<<0); // CS ON time clocks
         reg2 |= (csdelay<<7);        // CS half-clock delay
         reg2 |= (rdcsofftime<<8); // READ CS OFF time clocks
         reg2 |= (wrcsofftime<<16); // WRITE CS OFF time clocks

         uint32_t reg3 = 0;
         reg3 |= (0<<28);   // ADV_AAD_MUX_WR_OFF_TIME, ADV off time, AAD write
         reg3 |= (0<<24);   // ADV_AAD_MUX_RD_OFF_TIME, ADV off time, AAD read
         reg3 |= (wradvofftime<<16);  // ADV_WR_OFF_TIME, ADV off time, write
         reg3 |= (rdadvofftime<<8);   // ADV_RD_OFF_TIME, ADV off time, read
         reg3 |= (advdelay<<7);    // ADV half-clock delay
         reg3 |= (0<<4);    // ADV_AAD_MUX_ON_TIME, ADV on time, AAD cycle
         reg3 |= (advontime<<0);    // ADV_ON_TIME, ADV on time, normal cycle

         uint32_t reg4 = 0;
         reg4 |= (weofftime<<24);  // WE_OFF_TIME
         reg4 |= (0<<23);   // WE half-clock delay
         reg4 |= (weontime<<16);  // WE_ON_TIME
         reg4 |= (0<<13);   // OE_AAD_MUX_OFF_TIME, OE AAD MUX off time
         reg4 |= (oeofftime<<8);    // OE_OFF_TIME, OE off time
         reg4 |= (oedelay<<7);    // OE half-clock delay
         reg4 |= (0<<4);    // OE_AAD_MUX_ON_TIME, OE AAD MUX on time
         reg4 |= (oeontime<<0);    // OE_ON_TIME, OE on time

         uint32_t reg5 = 0;
         reg5 |= (1<<24); // PAGE_BURST_ACCESS_TIME
         reg5 |= (rdaccesstime<<16); // read access time
         reg5 |= (wrcycletime<<8); // write cycle time
         reg5 |= (rdcycletime<<0); // read cycle time

         uint32_t reg6 = 0;
         reg6 |= (wraccesstime<<24); // WR_ACCESS_TIME
         reg6 |= (wrdataonadmuxbus<<16); // WR_DATA_ON_AD_MUX_BUS, write data on AD mux bus

         gpmc_writeReg32(gpmc_regs,  0x60 + (0x30*cs), reg1); // config1
         gpmc_writeReg32(gpmc_regs,  0x64 + (0x30*cs), reg2); // config2
         gpmc_writeReg32(gpmc_regs,  0x68 + (0x30*cs), reg3); // config3
         gpmc_writeReg32(gpmc_regs,  0x6C + (0x30*cs), reg4); // config4
         gpmc_writeReg32(gpmc_regs,  0x70 + (0x30*cs), reg5); // config5
         gpmc_writeReg32(gpmc_regs,  0x74 + (0x30*cs), reg6); // config6
         gpmc_writeReg32(gpmc_regs,  0x78 + (0x30*cs), 0x00000f48+cs); // config7
      }

   return 0;
}

int gpmc_open()
{
   assert(gpmc_fd < 0);

   gpmc_fd = open("/dev/mem", O_RDWR|O_SYNC);

   if (gpmc_fd < 0) 
      gpmc_fd = open("/dev/gpmc_camac", O_RDWR|O_SYNC);

   if (gpmc_fd < 0) {
      printf("cannot open device file, errno %d (%s)\n", errno, strerror(errno));
      return -1;
   }

   ctrl_regs = (char*) mmap(0, ctrl_size, PROT_READ|PROT_WRITE, MAP_SHARED, gpmc_fd, ctrl_addr);

   if (ctrl_regs == (char*)-1) {
      printf("control module registers at address 0x%08x mapped at %p, errno %d (%s)\n", ctrl_addr, ctrl_regs, errno, strerror(errno));
      return -1;
   }

   gpmc_regs = (char*) mmap(0, regs_size, PROT_READ|PROT_WRITE, MAP_SHARED, gpmc_fd, gpmc_addr);
   
   if (gpmc_regs == (char*)-1) {
      printf("gpmc registers at address 0x%08x mapped at %p, errno %d (%s)\n", gpmc_addr, gpmc_regs, errno, strerror(errno));
      return -1;
   }

   gpmc_data = (char*) mmap(0, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, gpmc_fd, data_addr);

   if (gpmc_data == (char*)-1) {
      printf("gpmc data mapped at %p, errno %d (%s)\n", gpmc_data, errno, strerror(errno));
      return -1;
   }

   camac_data = (char*) mmap(0, camac_size, PROT_READ|PROT_WRITE, MAP_SHARED, gpmc_fd, camac_addr);

   if (camac_data == (char*)-1) {
      printf("gpmc camac data mapped at %p, errno %d (%s)\n", camac_data, errno, strerror(errno));
      return -1;
   }

   return 0;
}

int gpmc_close()
{
   assert(gpmc_fd >= 0);

   munmap(ctrl_regs, ctrl_size);
   munmap(gpmc_regs, regs_size);
   munmap(gpmc_data, data_size);
   munmap(camac_data, camac_size);

   close(gpmc_fd);

   gpmc_fd = -1;

   ctrl_regs = NULL;
   gpmc_regs = NULL;
   gpmc_data = NULL;
   camac_data = NULL;
         
   return 0;
}

// end of file
