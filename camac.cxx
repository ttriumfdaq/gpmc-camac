#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

extern "C" {
#include "libgpmc_camac.h"
}

int main(int argc, char* argv[])
{
   int err;

   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   err = gpmc_open();
   if (err != 0) {
      printf("Cannot open GPMC interface, gpmc_open() returned %d\n", err);
      exit(1);
   }

   if (argc == 2 && strcmp(argv[1], "--reboot")==0) {
      printf("CAMAC firmware: 0x%08x\n", gpmc_camac_readReg32(0));
      printf("rebooting FPGA...\n");
      gpmc_camac_writeReg32(1, 2); // command 2 reboot
      sleep(1);
      printf("CAMAC firmware: 0x%08x\n", gpmc_camac_readReg32(0));
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--scan")==0) {
      printf("Scan of CAMAC crate:\n");
      for (int n=1; n<=24; n++) {
         int x = 0;
         int q = 0;
         uint32_t read_data = 0;
         int loop = 0;
#if 0
         gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
         if (x || q)
            printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
         loop = 0;
         gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
         if (x || q)
            printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
         //sleep(1);
#endif
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--slot23")==0) {
      int n = 23;
      int x = 0;
      int q = 0;
      uint32_t read_data = 0;
      int loop = 0;
#if 1
      gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
      loop = 0;
      gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
#endif
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--slot24")==0) {
      int n = 24;
      int x = 0;
      int q = 0;
      uint32_t read_data = 0;
      int loop = 0;
#if 1
      gpmc_camac_NFAWMXQRL_mmap(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d (mmap)\n", n, x, q, read_data, loop);
#endif
#if 1
      loop = 0;
      gpmc_camac_NFAWMXQRL(n, 0, 0, 0, 0, &x, &q, &read_data, &loop);
      if (x || q)
         printf("N=%2d, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, x, q, read_data, loop);
#endif
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--test")==0) {
      while (1) {
         for (int write_data = 0; ; write_data++) {
            int n = 22;
            int x = 0;
            int q = 0;
            uint32_t read_data = 0;
            int loop = 0;
            gpmc_camac_NFAWMXQRL(n, 16, 0, ~write_data, 0, &x, &q, &read_data, &loop);
            int print = 0;
            if ((write_data%100000) == 0)
               print = 1;
            if (!q || !x)
               print = 1;
            if (loop > 4)
               print = 1;
            if (print)
               printf("N=%2d, X=%d, Q=%d, write=0x%08x, read=0x%08x, loop=%d\n", n, x, q, write_data, read_data, loop);
         }
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--gpmctest1")==0) {
      int ireg = 2;
      int errors = 0;
      for (int iloop=0; ; iloop++) {
         uint32_t w = random();
         gpmc_camac_writeReg32(2, w);
         uint32_t r = gpmc_camac_readReg32(2);
         uint32_t diff = w^r;
         if (diff != 0)
            errors++;
         if ((iloop==0) || diff || (iloop%1000000 == 0)) {
            printf("gpmctest register %d, loop %d, write 0x%08x, read 0x%08x, diff 0x%08x, errors %d\n", ireg, iloop, w, r, diff, errors);
            sleep(1);
         }
      }
      return 0;
   }

   if (argc == 2 && strcmp(argv[1], "--gpmctest2")==0) {
      int errors = 0;
      for (int iloop=0; ; iloop++) {
         uint32_t a = random();
         a &= 0x0FF8; // mask out: the top 4 bits is the register address, the low 2 bits are inaccessible, bit A[2] enables CAMAC operation.
         a |= (4<<12); // write to register 4
         uint32_t w = random();
         gpmc_camac_write32(a, w);
         uint32_t r4 = gpmc_camac_readReg32(4);
         uint32_t r5 = gpmc_camac_readReg32(5);
         a |= 2; // address bit 2 is always set (a 32-bit write is done as 2 16-bit writes to addresses a+0 and a+2. The last address latched.
         uint32_t diffa = a^r5;
         if (diffa != 0)
            errors++;
         uint32_t diffd = w^r4;
         if (diffd != 0)
            errors++;
         if ((iloop==0) || diffa || diffd || (iloop%1000000 == 0)) {
            printf("gpmctest camac registers, loop %d, write 0x%04x 0x%08x, read 0x%04x 0x%08x, diff 0x%04x 0x%08x, errors %d\n", iloop, a, w, r5, r4, diffa, diffd, errors);
            sleep(1);
         }
      }
      return 0;
   }

   if (argc == 1)
      {
         printf("register0: 0x%08x - CAMAC firmware revision\n", gpmc_camac_readReg32(0));
         printf("register1: 0x%08x - command register\n", gpmc_camac_readReg32(1));
         printf("register2: 0x%08x - read/write test register\n", gpmc_camac_readReg32(2));
         printf("register3: 0x%08x - flash programmer\n", gpmc_camac_readReg32(3));
         printf("register4: 0x%08x - CAMAC data readback\n", gpmc_camac_readReg32(4));
         printf("register5: 0x%08x - CAMAC address readback\n", gpmc_camac_readReg32(5));
         uint32_t r6 = gpmc_camac_readReg32(6);
         printf("register6: 0x%08x - CAMAC read data (0x%06x) and status (0x%02x)\n", r6, r6&0xFFFFFF, (r6>>24)&0xFF);
         uint32_t r7 = gpmc_camac_readReg32(7);
         printf("register7: 0x%08x - CAMAC LAM (0x%06x) and status (0x%02x)\n", r7, r7&0xFFFFFF, (r7>>24)&0xFF);
      }
   else if (argc == 2)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         //printf("addr 0x%08x\n", i);

         uint32_t v = gpmc_camac_readReg32(i);
         printf("camac register %d: 0x%08x\n", i, v);
         
      }
   else if (argc == 3)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         uint32_t v = strtoul(argv[2], NULL, 0);
         printf("write camac register %d, value 0x%08x\n", i, v);
         
         gpmc_camac_writeReg32(i, v);
      }
   else if (argc == 5)
      {
         int n = strtoul(argv[1], NULL, 0);
         int f = strtoul(argv[2], NULL, 0);
         int a = strtoul(argv[3], NULL, 0);
         int w = strtoul(argv[4], NULL, 0);

         int x = 0;
         int q = 0;
         uint32_t read_data = 0;
         int loop = 0;
         gpmc_camac_NFAWMXQRL(n, f, a, w, 0, &x, &q, &read_data, &loop);
         printf("N=%d, F=%d, A=%d, W=0x%08x, X=%d, Q=%d, data=0x%08x, loop=%d\n", n, f, a, w,  x, q, read_data, loop);
      }

   gpmc_close();
         
   return 0;
}
