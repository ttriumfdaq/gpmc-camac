#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

extern "C" {
#include "libgpmc_camac.h"
}

int main(int argc, char* argv[])
{
   int err;

   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   err = gpmc_open();
   if (err != 0) {
      printf("Cannot open GPMC interface, gpmc_open() returned %d\n", err);
      exit(1);
   }

   if (argc == 1)
      {
         gpmc_init();
         gpmc_print_status();
      }
   else if (argc == 2)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         //printf("addr 0x%08x\n", i);

         uint32_t v = gpmc_read32(i);
         printf("read addr 0x%08x: 0x%08x\n", i, v);
         
      }
   else if (argc == 3)
      {
         uint32_t i = strtoul(argv[1], NULL, 0);
         uint32_t v = strtoul(argv[2], NULL, 0);
         printf("write addr 0x%08x, value 0x%08x\n", i, v);
         
         gpmc_write32(i, v);
      }
   else if (0 && argc == 4)
      {
#if 0
         uint32_t i = strtoul(argv[1], NULL, 0);
         uint32_t v = strtoul(argv[2], NULL, 0);
         int test = strtoul(argv[3], NULL, 0);
         printf("addr 0x%08x, value 0x%08x, test %d\n", i, v, test);
         
         volatile uint32_t* p32 = (uint32_t*)(gpmc_data+i);
         volatile uint16_t* p16 = (uint16_t*)(gpmc_data+i);

         if (test == 1) {
            printf("Test 1: write -> write -> write\n");
            *p32 = v;
            *p32 = v;
            *p32 = v;
         } else if (test == 2) {
            printf("Test 2: write -> read\n");
            *p32 = v;
            uint32_t vv = *p32;
            printf("addr 0x%08x, value 0x%08x, test %d, read 0x%08x\n", i, v, test, vv);
         } else if (test == 3) {
            printf("Test 3: read -> read -> read\n");
            uint32_t vv1 = *p32;
            uint32_t vv2 = *p32;
            uint32_t vv3 = *p32;
            printf("addr 0x%08x, value 0x%08x, test %d, read 0x%08x 0x%08x 0x%08x\n", i, v, test, vv1, vv2, vv3);
         } else if (test == 4) {
            printf("Test 4: read -> write\n");
            uint32_t vv = *p32;
            *p32 = v;
            printf("addr 0x%08x, value 0x%08x, test %d, read 0x%08x\n", i, v, test, vv);
         } else if (test == 5) {
            printf("Test 5: read -> write -> read -> write\n");
            uint32_t vv1 = *p32;
            *p32 = v;
            uint32_t vv2 = *p32;
            *p32 = v;
            printf("addr 0x%08x, value 0x%08x, test %d, read 0x%08x 0x%08x\n", i, v, test, vv1, vv2);
         }
#endif
      }
   else if (0)
      {
#if 0
         while (1) {
            int addr = 0x0a000000;
            uint32_t* p32 = (uint32_t*)(gpmc_data+addr);
            uint32_t v1 = p32[0];
            uint32_t v2 = p32[1];
            uint32_t v3 = p32[2];
            uint32_t v4 = p32[3];
            printf("bus addr 0x%08x: 0x%08x 0x%08x 0x%08x 0x%08x\n", addr, v1, v2, v3, v4);
            sleep(1);
         }
         
         while (0) {
            uint32_t data = 0x12345678;
            int addr = 0x0a000000;
            uint32_t* p32 = (uint32_t*)(gpmc_data+addr);
            p32[0] = data;
            p32[1] = ~data;
            p32[2] = data;
            p32[3] = ~data;
            printf("bus addr 0x%08x: wrote 0x%08x\n", addr, data);
            sleep(1);
         }
#endif
#if 0
         while (0) {
            uint32_t cfg = readReg32(gpmc_regs, 0x50); // GPMC_CONFIG

            printf("GPMC config 0x%08x\n", cfg);

            while (1) {
               writeReg32(gpmc_regs, 0x50, cfg | (1<<4)); // WRITE_PROTECT
               printf("GPMC config 0x%08x (ON)\n", readReg32(gpmc_regs, 0x50));
               sleep(1);
               writeReg32(gpmc_regs, 0x50, cfg & ~(1<<4)); // WRITE_PROTECT
               printf("GPMC config 0x%08x (OFF)\n", readReg32(gpmc_regs, 0x50));
               sleep(1);
            }
         }
#endif
      }

#if 0
   if (0) {
      printReg("GPMC_ERR_ADDRESS", 0x44, gpmc_regs);
      printReg("GPMC_ERR_TYPE", 0x48, gpmc_regs);
   }
#endif

   gpmc_close();
         
   return 0;
}
