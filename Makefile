#
# Makefile for GPMC-CAMAC utilities using TI cross-compiler
#
# To build GPMC CAMAC tools using TI cross-compiler, run:
#   make
# To build using native compilers, run:
#   make CROSS_COMPILE=
#

XCOMMON := /daq/daqstore/olchansk/daq/MityARM/TI

XDIR5 := $(XCOMMON)/ti-sdk-am335x-evm-05.05.00.00/linux-devkit
XEXT5 := arm-arago-linux-gnueabi-

XDIR6 := $(XCOMMON)/ti-sdk-am335x-evm-06.00.00.00/linux-devkit/sysroots/i686-arago-linux
XEXT6 := arm-linux-gnueabihf-

CROSS_COMPILE := $(XDIR5)/bin/$(XEXT5)
#CROSS_COMPILE := $(XDIR6)/usr/bin/$(XEXT6)

#XCC  := $(CROSS_COMPILE)gcc
#XCXX := $(CROSS_COMPILE)g++

XCC  := arm-linux-gnueabihf-gcc-12
XCXX := arm-linux-gnueabihf-g++-12

FLAGS= -O2 -g -Wall -Wuninitialized

all:: libgpmc_camac.o
all:: gpmc
all:: camac
all:: srunner_gpmc

gpmc camac srunner_gpmc: libgpmc_camac.o

#%: %.c
#	$(XCC) -o $@ $(FLAGS) $< libgpmc_camac.o

%: %.cxx
	$(XCXX) -o $@ $(FLAGS) -static $< libgpmc_camac.o

%.o: %.c
	$(XCC) -c -o $@ $(FLAGS) $<

clean::
	-/bin/rm -f *.o

clean::
	-/bin/rm -f gpmc camac srunner_gpmc

#end
