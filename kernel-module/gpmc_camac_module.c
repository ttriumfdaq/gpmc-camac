/*
 * Name: gpmc_camac_module.c
 *
 * Kernel-side driver for the
 * TRIUMF CAMAC controller
 * FPGA connected through the GPMC bus
 *
 */

#include <linux/version.h>

#include <linux/types.h>
#include <linux/pci.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/poll.h>
#include <linux/mutex.h>

static int             ctrl_pfn = 0;
static char*           ctrl_regs = 0;

static int             gpmc_pfn = 0;
static char*           gpmc_regs = 0;

static char*           cs1_regs = 0;
static char*           cs2_regs = 0;
static char*           cs3_regs = 0;

static int             cs1_pfn = 0;
static int             cs2_pfn = 0;
static int             cs3_pfn = 0;

#ifdef HAVE_INTR
static u8    udev_irq = 0;
static int udev_intr_count = 0;
#endif

DEFINE_MUTEX(dev_mutex);

static int dev_open(struct inode * inode, struct file * file)
{
  printk(KERN_INFO MOD_NAME "::dev_open!\n");
  return 0;
}

static int dev_release(struct inode * inode, struct file * file)
{
  printk(KERN_INFO MOD_NAME "::dev_release!\n");
  return 0;
}

static int dev_mmap(struct file *file,struct vm_area_struct *vma)
{
  int err, pfn;

  /* mmap() arguments */
  u32 offset = vma->vm_pgoff << PAGE_SHIFT;
  u32 size   = vma->vm_end - vma->vm_start;

  printk(KERN_INFO MOD_NAME "::dev_mmap(0x%08x+0x%x)\n", offset, size);

  if (0) {
     printk(KERN_INFO MOD_NAME "::dev_mmap: start: 0x%08lx, end: 0x%08lx, vm_pgoff: 0x%lx, offset: 0x%x, size: 0x%x\n",
            vma->vm_start,
            vma->vm_end,
            vma->vm_pgoff,
            offset,
            size);
  }

  if (vma->vm_pgoff == ctrl_pfn && size == 0x2000) {
     pfn = ctrl_pfn;
     vma->vm_flags &= ~VM_WRITE;  /* remove write permission */
  } else if (vma->vm_pgoff == gpmc_pfn && size == 0x2000) {
     pfn = gpmc_pfn;
  } else if (vma->vm_pgoff == cs1_pfn && size == 0x3000000) {
     pfn = cs1_pfn;
  } else if (vma->vm_pgoff == cs1_pfn && size == 0x1000000) {
     pfn = cs1_pfn;
  } else if (vma->vm_pgoff == cs2_pfn && size == 0x1000000) {
     pfn = cs2_pfn;
  } else if (vma->vm_pgoff == cs3_pfn && size == 0x1000000) {
     pfn = cs3_pfn;
  } else {
     printk(KERN_INFO MOD_NAME "::dev_mmap: invalid mmap addresses: start: 0x%08lx, end: 0x%08lx, vm_pgoff: 0x%lx, offset: 0x%x, size: 0x%x\n",
            vma->vm_start,
            vma->vm_end,
            vma->vm_pgoff,
            offset,
            size);
     return -EIO;
  }

  /* MMIO pages should be non-cached */
  vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

  /* Don't dump addresses that are not real memory to a core file. */
  vma->vm_flags |= VM_IO;

  /* remove execute permission */
  vma->vm_flags &= ~VM_EXEC;

  err = remap_pfn_range(vma, vma->vm_start, pfn, vma->vm_end - vma->vm_start, vma->vm_page_prot);
  if (err) {
     printk(KERN_ERR MOD_NAME "::dev_mmap: remap_pfn_range(0x%x) error %d\n", pfn, err);
     return -EIO;
  }

  return 0;
}

// read and write 32-bit data from registers in the CAMAC interface

static void gpmc_camac_write32(uint32_t avalue, uint32_t dvalue)
{
   avalue &= 0x1FFFE;
   iowrite32(dvalue, cs3_regs + avalue);
}

#if 0
static void gpmc_camac_writeReg32(int ireg, uint32_t value)
{
   iowrite32(value, cs3_regs + (ireg<<12));
}
#endif

static uint32_t gpmc_camac_readReg32(int ireg)
{
   return ioread32(cs3_regs + (ireg<<12));
}

static long ioctl1(int nbytes, unsigned long arg)
{
   u32 buf[9];

   //printk(KERN_INFO MOD_NAME "::ioctl1(nbytes=%d)\n", nbytes);

   if (nbytes != sizeof(buf)) {
      printk(KERN_INFO MOD_NAME "::ioctl1(nbytes=%d): invalid byte count: should be %d, got %d\n", nbytes, sizeof(buf), nbytes);
      return -EINVAL;
   }

   if (copy_from_user(buf, (void *)arg, sizeof(buf)))
      return -EFAULT;

   // *bufp++ = n;
   // *bufp++ = f;
   // *bufp++ = a;
   // *bufp++ = write_data;
   // *bufp++ = loop_max;
   // resp = bufp;
   // *bufp++ = 0; // x
   // *bufp++ = 0; // q
   // *bufp++ = 0; // read_data
   // *bufp++ = 0; // loop counter

   {
      int n = buf[0];
      int f = buf[1];
      int a = buf[2];
      int write_data = buf[3];
      int loop_max = buf[4];

      int trace = 0;

      int xx = 0;
      int xloop[10];
      int xrvalue[10];
      
      int i;
      int avalue = 0;
      uint32_t dvalue = 0;
      uint32_t rvalue = 0;
      uint32_t rr = 0;
      int q;
      int x;

      n &= 0x1F;
      f &= 0x1F;
      a &= 0xF;
      write_data &= 0xFFFFFF;

      avalue |= 0x4000;
      avalue |= (n<<4);
      avalue |= (f&0x10)>>1; // F16
      avalue |= 1<<2; // "go" bit
      
      dvalue |= write_data;
      dvalue |= a<<24;
      dvalue |= (f&0xF)<<28;

      if (loop_max < 0) {
         trace = 1;
         loop_max = 10;
      }

      if (loop_max <= 0)
         loop_max = 100;

      //if (loop_max > 10)
      //   loop_max = 10;

      if (trace)
         printk(KERN_INFO MOD_NAME "::ioctl1: camac operation 0x%08x 0x%08x, trace %d, loop_max %d\n", avalue, dvalue, trace, loop_max);

      gpmc_camac_write32(avalue, dvalue);
      
      //rr = gpmc_camac_readReg32(6);
      
      for (i=1; i<loop_max; i++) {
         rvalue = gpmc_camac_readReg32(6);
         if (1) {
            if (rvalue != rr) {
               xloop[xx] = i;
               xrvalue[xx] = rvalue;
               if (xx<9)
                  xx++;
               //printk(KERN_INFO MOD_NAME "::ioctl1: loop %d rvalue 0x%08x\n", i, rvalue);
            }
            rr = rvalue;
         }
         if ((rvalue & 0x04000000))
            break;
      }

      if (trace)
         for (i=0; i<xx; i++) {
            printk(KERN_INFO MOD_NAME "::ioctl1: entry %d, loop %d, rvalue 0x%08x\n", i, xloop[i], xrvalue[i]);
         }
      
      if (rvalue & (1<<24))
         x = 1;
      else
         x = 0;
      
      if (rvalue & (1<<25))
         q = 1;
      else
         q = 0;

      buf[5] = x;
      buf[6] = q;
      buf[7] = rvalue;
      buf[8] = i;
   }

   if (copy_to_user((void *)arg, buf, sizeof(buf)))
      return -EFAULT;

   return 0;
}

static long dev_unlocked_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
   int status = 0;
   int xcmd = cmd>>16;
   int xbytes = cmd&0xFFFF;
   //printk(KERN_INFO MOD_NAME "::dev_unlocked_ioctl(xcmd=%d,xbytes=%d)\n", xcmd, xbytes);

   if (xcmd == 1) {
      mutex_lock(&dev_mutex);
      status = ioctl1(xbytes, arg);
      mutex_unlock(&dev_mutex);
   } else {
      printk(KERN_INFO MOD_NAME "::dev_unlocked_ioctl(xcmd=%d,xbytes=%d): error: unknown command\n", xcmd, xbytes);
      status = -EIO;
   }

   return status;
}

static struct file_operations dev_fops = {
  mmap:		dev_mmap,
  unlocked_ioctl: dev_unlocked_ioctl,
  open:		dev_open,
  release:	dev_release
};

#ifdef HAVE_INTR
static int intr_handler(int irq, void *dev_id, struct pt_regs *regs)
{
  int mine;
  u32 stat, statr;

#if 0
  stat = ioread32(udev_softdac_regs + 0x80010);
  mine = stat & 0x70000000;
  if (mine)
    {
      //plx9080_intr_show();
      iowrite8(0x70, udev_softdac_regs + 0x80013);
      // issue a PCI read to flush posted PCI writes
      statr = ioread32(udev_softdac_regs + 0x80010);
      printk(KERN_INFO MOD_NAME ": interrupt irq %d, stat 0x%08x -> 0x%08x, count %d\n", irq, stat, statr, udev_intr_count++);
      //plx9080_intr_show();
      return IRQ_HANDLED;
    }
#endif

  printk(KERN_INFO MOD_NAME ": (for somebody else...) interrupt irq %d, stat 0x%08x, count %d\n", irq, stat, udev_intr_count++);

  return IRQ_NONE;
}
#endif

int printReg(const char* name, uint32_t offset, char* base)
{
   //uint32_t value = *(uint32_t*)(base+offset);
   uint32_t value = ioread32(base+offset);
   printk(KERN_INFO MOD_NAME ": offset 0x%04x: %-25s: 0x%08x\n", offset, name, value);
   return value;
}

int printPadReg(const char* name, uint32_t offset, char* base)
{
   char buf[256];
   //uint32_t value = *(uint32_t*)(base+offset);
   uint32_t value = ioread32(base+offset);
   sprintf(buf, "offset 0x%04x: %-25s: 0x%08x: ", offset, name, value);
   sprintf(buf+strlen(buf), "func %d", value & 0x7);
   sprintf(buf+strlen(buf), ", PULLUDEN: %d", value & (1<<3));
   sprintf(buf+strlen(buf), ", PULLTYPESEL: %d", value & (1<<4));
   sprintf(buf+strlen(buf), ", RXACTIVE: %d", value & (1<<5));
   sprintf(buf+strlen(buf), ", SLEWCTRL: %d", value & (1<<6));
   printk(KERN_INFO MOD_NAME ": %s\n", buf);
   return value;
}


/*
 * the initialisation routine.
 * Return: 0 if okey, -Exxx if error.
 */

int init_module(void)
{
  int start, len;
  int i;

  printk(KERN_INFO MOD_NAME ": Linux driver by K.Olchanski/TRIUMF\n");
  printk(KERN_INFO MOD_NAME ": driver compiled " __DATE__ " " __TIME__ "\n");

  mutex_init(&dev_mutex);

  if (register_chrdev(MOD_MAJOR, MOD_NAME, &dev_fops)) {
    printk(KERN_ERR MOD_NAME ": Cannot register device\n");
    return -ENODEV;
  }

  start = 0x44E10000;
  len   = 0x00002000;

  ctrl_pfn = start >> PAGE_SHIFT;
  ctrl_regs = ioremap(start, len);
        
  printk(KERN_INFO MOD_NAME ": control module registers at bus address 0x%x mapped to local addr 0x%p, length 0x%x, pfn 0x%x\n", start, ctrl_regs, len, ctrl_pfn);

  start = 0x50000000;
  len   = 0x00002000;

  gpmc_pfn = start >> PAGE_SHIFT;
  gpmc_regs = ioremap(start, len);
        
  printk(KERN_INFO MOD_NAME ": GPMC registers at bus address 0x%x mapped to local addr 0x%p, length 0x%x, pfn 0x%x\n", start, gpmc_regs, len, gpmc_pfn);

  len = 0x01000000;

  start = 0x09000000;

  if (!request_mem_region(start, len, "gpmc_camac_CS1")) {
     printk (KERN_ERR MOD_NAME ": Error: Cannot allocate GPMC CS1 memory region\n");
     return -ENODEV;
  }

  cs1_pfn = start >> PAGE_SHIFT;
  cs1_regs = ioremap(start, len);
        
  printk(KERN_INFO MOD_NAME ": GPMC CS1 bus address 0x%x mapped to local addr 0x%p, length 0x%x, pfn 0x%x\n", start, cs1_regs, len, cs1_pfn);

  start = 0x0A000000;

  if (!request_mem_region(start, len, "gpmc_camac_CS2")) {
     printk (KERN_ERR MOD_NAME ": Error: Cannot allocate GPMC CS2 memory region\n");
     return -ENODEV;
  }

  cs2_pfn = start >> PAGE_SHIFT;
  cs2_regs = ioremap(start, len);
        
  printk(KERN_INFO MOD_NAME ": GPMC CS2 bus address 0x%x mapped to local addr 0x%p, length 0x%x, pfn 0x%x\n", start, cs2_regs, len, cs2_pfn);

  start = 0x0B000000;

  if (!request_mem_region(start, len, "gpmc_camac_CS3")) {
     printk (KERN_ERR MOD_NAME ": Error: Cannot allocate GPMC CS3 memory region\n");
     return -ENODEV;
  }

  cs3_pfn = start >> PAGE_SHIFT;
  cs3_regs = ioremap(start, len);
        
  printk(KERN_INFO MOD_NAME ": GPMC CS3 bus address 0x%x mapped to local addr 0x%p, length 0x%x, pfn 0x%x\n", start, cs3_regs, len, cs3_pfn);

  printk(KERN_INFO MOD_NAME ": Control Module registers:\n");
  
  printReg("control_revision",      0, ctrl_regs);
  printReg("device_id",         0x600, ctrl_regs);
  printReg("control_hwinfo",     0x04, ctrl_regs);
  printReg("control_sysconfig",  0x10, ctrl_regs);
  printReg("control_status",     0x40, ctrl_regs);

  printk(KERN_INFO MOD_NAME ": Programming GPMC pinmux registers...\n");

  //
  // OMAP pin multiplexor:
  //
  // see spruh73g.pdf page 754 "control module"
  //
  // Table 9-1. Pad Control Register Field Descriptions
  //
  // Bit Field Value Description
  //  31-7 Reserved Reserved. Read returns 0.
  //     6 SLEWCTRL Select between faster or slower slew rate: 0 Fast, 1 Slow(1)
  //     5 RXACTIVE Input enable value for the Pad. Set to 0 for output only. Set to 1 for input or output.
  //     4 PULLTYPESEL Pad pullup/pulldown type selection: 0 Pulldown selected, 1 Pullup selected
  //     3 PULLUDEN Pad Pullup/pulldown enable: 0 Pullup/pulldown enabled,  1 Pullup/pulldown disabled.
  //   2-0 MUXMODE Pad functional signal mux select
  //

  iowrite32(0x30, ctrl_regs + 0x800); // conf_gpmc_ad0
  iowrite32(0x30, ctrl_regs + 0x804); // conf_gpmc_ad1
  iowrite32(0x30, ctrl_regs + 0x808); // conf_gpmc_ad2
  iowrite32(0x30, ctrl_regs + 0x80C); // conf_gpmc_ad3
  iowrite32(0x30, ctrl_regs + 0x810); // conf_gpmc_ad4
  iowrite32(0x30, ctrl_regs + 0x814); // conf_gpmc_ad5
  iowrite32(0x30, ctrl_regs + 0x818); // conf_gpmc_ad6
  iowrite32(0x30, ctrl_regs + 0x81C); // conf_gpmc_ad7
  iowrite32(0x30, ctrl_regs + 0x820); // conf_gpmc_ad8
  iowrite32(0x30, ctrl_regs + 0x824); // conf_gpmc_ad9
  iowrite32(0x30, ctrl_regs + 0x828); // conf_gpmc_ad10
  iowrite32(0x30, ctrl_regs + 0x82C); // conf_gpmc_ad11
  iowrite32(0x30, ctrl_regs + 0x830); // conf_gpmc_ad12
  iowrite32(0x30, ctrl_regs + 0x834); // conf_gpmc_ad13
  iowrite32(0x30, ctrl_regs + 0x838); // conf_gpmc_ad14
  iowrite32(0x30, ctrl_regs + 0x83C); // conf_gpmc_ad15

  iowrite32(0x8,  ctrl_regs + 0x878); // conf_gpmc_ben1

  //iowrite32(0x8,  ctrl_regs + 0x87C); // conf_gpmc_csn0 - on-board NAND flash
  iowrite32(0x8,  ctrl_regs + 0x880); // conf_gpmc_csn1
  iowrite32(0x8,  ctrl_regs + 0x884); // conf_gpmc_csn2
  iowrite32(0x8,  ctrl_regs + 0x888); // conf_gpmc_csn3 - cyclone-4 FPGA camac interface

  iowrite32(0x30, ctrl_regs + 0x88C); // conf_gpmc_clk

  printk(KERN_INFO MOD_NAME ": Done programming GPMC pinmux registers.\n");

  printk(KERN_INFO MOD_NAME ": GPMC pinmux registers:\n");

  for (i=0x800; i<=0x89C; i+=4) {
     printPadReg("config_gpmc", i, ctrl_regs);
  }

  printk(KERN_INFO MOD_NAME ": Programming GPMC registers...\n");

  iowrite32(((0x1FF<<4)|1), gpmc_regs + 0x40); // enable timeout

  if (1) {
     int cs = 3; // CAMAC interface is on GPMC CS3
  
     uint32_t reg1 = 0;
     reg1 |= (0<<31); // WRAP_BURST
     reg1 |= (1<<30); // READ_MULTIPLE
     reg1 |= (1<<29); // readtype: 0=async, 1=sync
     reg1 |= (0<<28); // WRITE_MULTIPLE
     reg1 |= (1<<27); // writetype: 0=async, 1=sync
     reg1 |= (0<<25); // CLK activation time
     reg1 |= (0<<23); // attached device burst length
     reg1 |= (1<<12); // 0=8bit, 1=16bit
     reg1 |= (2<<8); // 0=nonmux, 1=AADmux, 2=ADmux, 3=reserved
     reg1 |= (0<<0); // CLK divider
     
     int wraccesstime = 0; // not used?
     int weontime = 0; // not used?
     int weofftime = 0; // not used?
     
     // timing of address phase
     int csontime = 0;
     int csdelay = 0; // CS 1/2 clock delay
     int advontime = csontime + 1;
     int advdelay = 0; // ADV 1/2 clock delay
     
     // timing of write cycle
     int wrdataonadmuxbus = csontime + 1; // switch from driving address to driving data
     int wradvofftime = wrdataonadmuxbus + 1;
     int wrcsofftime = wrdataonadmuxbus + 1;
     int wrcycletime = wrcsofftime + 0;
     
     // timing of read cycle
     int oeontime = csontime + 1; // switch from driving address to reading data (GPMC bus turn around)
     int oedelay = 1; // delay OE by 1/2 clock
     int rdaccesstime = oeontime + 4;
     int oeofftime = rdaccesstime + 0;
     int rdcsofftime = oeofftime + 1;
     int rdadvofftime = rdcsofftime + 0;
     int rdcycletime = rdcsofftime + 0;
     
     printk(KERN_INFO MOD_NAME ": GPMC CS %d, read cycle %d GPMC clocks, write cycle %d GPMC clocks\n", cs, rdcycletime, wrcycletime);
     
     uint32_t reg2 = 0;
     reg2 |= (csontime<<0); // CS ON time clocks
     reg2 |= (csdelay<<7);        // CS half-clock delay
     reg2 |= (rdcsofftime<<8); // READ CS OFF time clocks
     reg2 |= (wrcsofftime<<16); // WRITE CS OFF time clocks
     
     uint32_t reg3 = 0;
     reg3 |= (0<<28);   // ADV_AAD_MUX_WR_OFF_TIME, ADV off time, AAD write
     reg3 |= (0<<24);   // ADV_AAD_MUX_RD_OFF_TIME, ADV off time, AAD read
     reg3 |= (wradvofftime<<16);  // ADV_WR_OFF_TIME, ADV off time, write
     reg3 |= (rdadvofftime<<8);   // ADV_RD_OFF_TIME, ADV off time, read
     reg3 |= (advdelay<<7);    // ADV half-clock delay
     reg3 |= (0<<4);    // ADV_AAD_MUX_ON_TIME, ADV on time, AAD cycle
     reg3 |= (advontime<<0);    // ADV_ON_TIME, ADV on time, normal cycle
     
     uint32_t reg4 = 0;
     reg4 |= (weofftime<<24);  // WE_OFF_TIME
     reg4 |= (0<<23);   // WE half-clock delay
     reg4 |= (weontime<<16);  // WE_ON_TIME
     reg4 |= (0<<13);   // OE_AAD_MUX_OFF_TIME, OE AAD MUX off time
     reg4 |= (oeofftime<<8);    // OE_OFF_TIME, OE off time
     reg4 |= (oedelay<<7);    // OE half-clock delay
     reg4 |= (0<<4);    // OE_AAD_MUX_ON_TIME, OE AAD MUX on time
     reg4 |= (oeontime<<0);    // OE_ON_TIME, OE on time
     
     uint32_t reg5 = 0;
     reg5 |= (1<<24); // PAGE_BURST_ACCESS_TIME
     reg5 |= (rdaccesstime<<16); // read access time
     reg5 |= (wrcycletime<<8); // write cycle time
     reg5 |= (rdcycletime<<0); // read cycle time
     
     uint32_t reg6 = 0;
     reg6 |= (wraccesstime<<24); // WR_ACCESS_TIME
     reg6 |= (wrdataonadmuxbus<<16); // WR_DATA_ON_AD_MUX_BUS, write data on AD mux bus

     uint32_t reg7 = 0x00000f48+cs;
     iowrite32(reg1, gpmc_regs + 0x60 + (0x30*cs)); // config1
     iowrite32(reg2, gpmc_regs + 0x64 + (0x30*cs)); // config2
     iowrite32(reg3, gpmc_regs + 0x68 + (0x30*cs)); // config3
     iowrite32(reg4, gpmc_regs + 0x6C + (0x30*cs)); // config4
     iowrite32(reg5, gpmc_regs + 0x70 + (0x30*cs)); // config5
     iowrite32(reg6, gpmc_regs + 0x74 + (0x30*cs)); // config6
     iowrite32(reg7, gpmc_regs + 0x78 + (0x30*cs)); // config7
  }

  printk(KERN_INFO MOD_NAME ": Done programming GPMC registers.\n");

  printk(KERN_INFO MOD_NAME ": GPMC registers:\n");

  printReg("GPMC_REVISION",             0, gpmc_regs);
  printReg("GPMC_SYSCONFIG",        0x010, gpmc_regs);
  printReg("GPMC_SYSSTATUS",        0x014, gpmc_regs);
  printReg("GPMC_IRQSTATUS",        0x018, gpmc_regs);
  printReg("GPMC_IRQENABLE",        0x01C, gpmc_regs);
  printReg("GPMC_TIMEOUT_CONTROL",  0x040, gpmc_regs);
  printReg("GPMC_ERR_ADDRESS",      0x044, gpmc_regs);
  printReg("GPMC_ERR_TYPE",         0x048, gpmc_regs);
  printReg("GPMC_CONFIG",           0x050, gpmc_regs);
  printReg("GPMC_STATUS",           0x054, gpmc_regs);
  printReg("GPMC_PREFETCH_CONFIG1", 0x1e0, gpmc_regs);
  printReg("GPMC_PREFETCH_CONFIG2", 0x1e4, gpmc_regs);
  printReg("GPMC_PREFETCH_CONTROL", 0x1ec, gpmc_regs);
  printReg("GPMC_PREFETCH_STATUS",  0x1f0, gpmc_regs);

  {
     int cs;
     for (cs=3; cs<=3; cs++) {
        printk(KERN_INFO MOD_NAME ": GPMC CS%d:\n", cs);
        printReg("GPMC_CONFIG1_i", 0x60 + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG2_i", 0x64 + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG3_i", 0x68 + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG4_i", 0x6C + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG5_i", 0x70 + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG6_i", 0x74 + (0x30*cs), gpmc_regs);
        printReg("GPMC_CONFIG7_i", 0x78 + (0x30*cs), gpmc_regs);
     }
  }
  
#if 0
  if (0)
    {
      int i;
      for (i=0; i<=0x10; i+=4)
	{
           iowrite32(0xdeadbeef, udev_regs + i);
           ioread32(udev_regs + i);
	}
    }
#endif

#ifdef HAVE_INTR
  udev_irq = pdev->irq;
  printk(KERN_INFO MOD_NAME ": Using irq %d\n", udev_irq);

  /* Connect interrupts */
  if (udev_irq > 0)
    {
      int err;
      /* we may use shared interrupts */
      err = request_irq(udev_irq, intr_handler, SA_INTERRUPT|SA_SHIRQ, MOD_NAME, udev_pdev);
      if (err)
	{
	  printk(KERN_ERR MOD_NAME ": Cannot get irq %d, err %d\n", udev_irq, err);
	  udev_irq = 0;
	}

      enable_irq(udev_irq);

      // This enables the PMC-SOFTDAC-M interrupts -
      // commented out because this driver does not
      // do anything with them - we should only enable
      // interrupts when a user program is ready to do
      // something with them. We should call plx9080_intr_enable()
      // in the open() call, or in the mmap() call. or in a
      // "wait for interrupt" call.
      //plx9080_intr_enable();
    }
#endif

  printk(KERN_INFO MOD_NAME ": driver init done\n");

  return 0;
}

void cleanup_module()
{
  printk(KERN_INFO MOD_NAME ": cleanup_module\n");

  if (ctrl_regs)
     iounmap(ctrl_regs);

  if (gpmc_regs)
     iounmap(gpmc_regs);

  if (cs1_regs)
     iounmap(cs1_regs);

  if (cs2_regs)
     iounmap(cs2_regs);

  if (cs3_regs)
     iounmap(cs3_regs);

  release_mem_region(0x09000000, 0x01000000);
  release_mem_region(0x0A000000, 0x01000000);
  release_mem_region(0x0B000000, 0x01000000);

  unregister_chrdev(MOD_MAJOR, MOD_NAME);

  printk(KERN_INFO MOD_NAME ": driver removed\n");
}

/* end file */
