//
// libgpmc_camac.h
//
// Konstantin Olchanski, TRIUMF, 21-JUNE-2014
//

#ifndef INCLUDE_LIBGPMC_CAMAC_H
#define INCLUDE_LIBGPMC_CAMAC_H

#include <stdint.h>

int gpmc_open();
int gpmc_init();
int gpmc_close();

uint32_t gpmc_read32(int addr);
void     gpmc_write32(int addr, uint32_t value);

uint32_t gpmc_camac_readReg32(int ireg);
void gpmc_camac_writeReg32(int ireg, uint32_t value);
void gpmc_camac_write32(uint32_t avalue, uint32_t dvalue);

void gpmc_camac_NFAWMXQRL_mmap(int n, int f, int a, int write_data, int loop_max, int* x, int* q, uint32_t *read_data, int* loop);
void gpmc_camac_NFAWMXQRL(int n, int f, int a, int write_data, int loop_max, int* x, int* q, uint32_t *read_data, int* loop);

void gpmc_print_status();

#endif

// end of file
