# enable GPMC pins

echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad8      ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad9      ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad10     ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad11     ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad12     ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad13     ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad14     ### AD pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_ad15     ### AD pin

echo 8 >> /sys/kernel/debug/omap_mux/gpmc_csn1      ### CS pin
echo 8 >> /sys/kernel/debug/omap_mux/gpmc_csn2      ### CS pin
echo 8 >> /sys/kernel/debug/omap_mux/gpmc_csn3      ### CS pin

echo 8 >> /sys/kernel/debug/omap_mux/gpmc_ben1      ### output pin
echo 30 >> /sys/kernel/debug/omap_mux/gpmc_clk      ### CLK has to be configured as in "input" pin

#end
